import Data.List

panjang [] = 0
panjang (x:xs) = 1 + panjang xs

panjang1 x = 1

--definisi fungsi map
map1 :: (a -> b) -> [a] -> [b]  
map1 _ [] = []  
map1 f (x:xs) = f x : map1 f xs  

-- menghitung panjang list menggunakan map dan sum
panjang2 x = panjang (map1 panjang1 x) 

-- mengkomposisikan fungsi f sebanyak n kali
iter :: Int -> (Int -> Int) -> Int -> Int
iter 1 f x = f x
iter n f x = f (iter (n-1) f x) 

--definisi fungsi succ
succe :: Int -> Int
succe x = (+1) x

--definisi fungsi zip
zip1 :: [a] -> [b] -> [(a,b)]
zip1 (x:xs) (y:ys) = (x,y) : zip1 xs ys
zip1 _ _ = []

--definisi fungsi zipwith
zipwith1 :: (a->b -> c) -> [a] -> [b] -> [c]
zipwith1 f (x:xs) (y:ys) = f x y : zipwith1 f xs ys
zipwith1 f _ _ = []

--definisi fungsi filter
filter1 :: (a -> Bool) -> [a] -> [a]
filter1 f [] = []
filter1 f (x:xs)
	| f x       = x : filter1 f xs
	| otherwise = filter1 f xs

-- definisi fungsi filter dalam bentuk list comprehension
filter2 f xs = [x | x <- xs, f x]

-- memfilter list genap
isEven :: Int -> Bool
isEven x 
	| mod x 2 == 0  = True
	| otherwise     = False

isGenap xs = filter1 isEven xs

-- menjumlahkan (1..n)^2
jumlah :: Int -> [Int]
jumlah 1 = [1]
jumlah n = ([n]++(jumlah(n-1)))
jumlahArray x = map (^2) (jumlah x)
lipat x = foldr1 (+) (jumlahArray x)

--fungsi flip yang membalikkan posisi argumen
flip10 :: (a -> b -> c) -> (b -> a -> c)
flip10 f x y = f y x

--latihan sederhana list comprehension
saring7 xs=[x | x<-xs, x>7]
tambah3 xs= [x+3|x<-xs, x/=1]
tambah5 xs = [ x+5 | Just x<-xs]

-- bentuk lain dari [ x+4 | (x,y) <- xys, x+y < 5 ]
saring1 xs = map (\(x,y) -> x+4) (filter (\(x,y) -> x+y <5) xs)

-- faktor dari n
divisor1 n = [x | x<-[1..n], mod n x==0 ]

-- definisi fungsi foldr
foldr6 :: (a -> b -> b) -> b -> [a] -> b
foldr6 f x []    = x
foldr6 f x (y:ys) = f y (foldr6 f x ys)

-- menginsert elemen sesuai urutan list
ins1 x [] = [x]
ins1 x (y:ys)
	| x<=y = (y:x:ys)
	| otherwise = y : ins1 x ys

--melakukan reverse
reve :: [Int] -> [Int]
reve [] = []
reve (x:xs) = reve xs ++ [x]

-- menjalankan fungsi max
max4 :: Int -> Int -> Int -> Int
max4 a b c 
	| a > b && b > c = a
	| a < b && b < c = c
	| otherwise = b