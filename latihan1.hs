
-- menjawab soal nomor 1
map1 :: (a -> b) -> [a] -> [b]  
map1 _ [] = []  
map1 f (x:xs) = f x : map1 f xs  

foldr3 f x [] = x
foldr3 f x (y:ys) = f  y (foldr3 f x ys)

-- fungsi map dalam bentuk list comprehension
map2 f xs= [f x | x<-xs]

data Expr = C Float | Expr :+ Expr | Expr :- Expr | Expr :* Expr | Expr :/ Expr
			| V String | Let String Expr Expr
			deriving Show


-- code subst dan evaluate disalin untuk mempermudah
-- memahami cara kerja fungsi

subst :: String -> Expr -> Expr -> Expr

subst v0 e0 (V v1)         = if (v0 == v1) then e0 else (V v1)
subst v0 e0 (C c)          = (C c)
subst v0 e0 (e1 :+ e2)     = subst v0 e0 e1 :+ subst v0 e0 e2
subst v0 e0 (e1 :- e2)     = subst v0 e0 e1 :- subst v0 e0 e2
subst v0 e0 (e1 :* e2)     = subst v0 e0 e1 :* subst v0 e0 e2
subst v0 e0 (e1 :/ e2)     = subst v0 e0 e1 :/ subst v0 e0 e2
subst v0 e0 (Let v1 e1 e2) = Let v1 e1 (subst v0 e0 e2)



evaluate (C x) = x

evaluate (e1 :+ e2)    = (evaluate e1) + (evaluate e2)
evaluate (e1 :- e2)    = (evaluate e1) - (evaluate e2)
evaluate (e1 :* e2)    = (evaluate e1 * evaluate e2)
evaluate (e1 :/ e2)    = (evaluate e1 / evaluate e2)
evaluate (Let v e0 e1) = evaluate (subst v e0 e1)

-- mengatasi problem evaluate ketika operator berhadapan (V _)
evaluate ((V _) :* e)  = evaluate (C 0.0 :+ e)   
evaluate (e :* (V _))  = evaluate (e :+ C 0.0)
evaluate ((V _) :/ e)  = evaluate (C 0.0 :+ e)
evaluate (e :/ (V _))   = evaluate (e :+ C 0.0)
evaluate ((V _) :+ e)  = evaluate (C 0.0 :+ e)
evaluate (e :+ (V _))   = evaluate (e :+ C 0.0)
evaluate ((V _) :- e)  = evaluate (C 0.0 :+ e)
evaluate (e :- (V _))   = evaluate (e :+ C 0.0)



-- sepertinya bakal berguna
panjang5 :: Expr -> Int
panjang5 (V xs) = length xs

--menyimpan ekspresi
  	saveExpr :: Expr -> [Expr]
 	saveExpr (C e1 :+ C e2)  = [e1,+,e2]
 	saveExpr (C e1 :- C e2)  = [e1,-,e2]
 	saveExpr (C e1 :* C e2)  = [e1,*,e2]
 	saveExpr (C e1 :/ C e2)  = [e1,/,e2]


 
-- masih bingung mencari fungsi agar bisa difold
eval1 :: Expr -> Float
eval1 (C x) = x
eval1 (e1 :+ e2)    = e1 +  e2
eval1 (e1 :- e2)    = e1 -  e2
eval1 (e1 :* e2)    = e1 *  e2
eval1 (e1 :/ e2)    = e1 / e2
eval1 (Let v e0 e1) = (subst v e0 e1)
eval1 (V v)         = 0.0   

-- mengumpulkan karakter dari expr dalam bentuk list
kumpulanExpr e1 e2 = saveExpr e1 ++ saveExpr e2

--melipat kumpulanExpr 
--foldr1 merupakan function built in haskell
foldExpr = foldr1 eval1 kumpulanExpr


-- MENJAWAB SOAL NOMOR 2 KASUS IMPERATIVE ROBOT
-- tipe dari fungsi mapM_ ialah Monad m =>(a -> m b) -> [a] -> m()
-- artinya fungsi ini akan memetakan seluruh list a yang akan mengubah
-- nilai namun tidak mengembalikan nilai

-- menjawab pertanyaan nomor 3 terkait forloop
forloop :: [Int] -> Robot () -> [Robot ()]
forloop xs f = mapM_ (const f) xs

-- eval+ x _ = eval1
-- eval2 xs = foldr1 eval+ 0 xs 

-- Menjalankan fungsi kpk
kpk :: Int -> Int -> Int
kpk x y = kpkLain x y x y

kpkLain:: Int -> Int -> Int -> Int -> Int
kpkLain x y a b
	| x == y = x
	| x < y  = kpkLain (x+a) y a b
	| x >  y = kpkLain x (y+b) a b


-- insert1 [x,y] y
-- 	| x > y     = [y,x]
-- 	| otherwise = [x,y]

-- insert2 xs n x   = insert1 (xs) (xs !! (n-1)) ++ insert1 xs (xs !! n)

